const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require("dotenv");
const cors = require('cors');

dotenv.config();


//Connect To DB
mongoose.connect(process.env.DB_CONNECT,{ 
    useNewUrlParser: true,
    useUnifiedTopology: true
},() => console.log("Connected To DB"));

//middleware
app.use(cors());
app.use(express.json());

//Import Routes
const authRoute = require('./routes/auth');
const postRoute = require('./routes/post');

//Route Middlewares
app.use('/api/user' , authRoute);
app.use('/api/posts' , postRoute);

const port = process.env.port || 5000;
app.listen(port , () => console.log("Server Up And Running On Port "+port))