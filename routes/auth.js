const router = require('express').Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../model/User");

const {registerValidation , loginValidation} = require("../validation");

router.post('/register' , async (req , res) => {
    //Check Vaildation Data
    const { error, value } = registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);


    //Check Email Exists
    const emailExists = await User.findOne({email: value.email});
    if(emailExists) return  res.status(400).send("Email Already Exists");

    //Encrypt Password
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(value.password , salt);

    const user = new User({
        name: value.name,
        email: value.email,
        password: password,
    });

    try{
        const savedUser = await user.save();
        res.send({
            user: savedUser._id
        });
    }catch(err){
        res.status(400).send(err);
    }
    return res.status(200).send("HHHHHHI")
});

router.post('/login', async (req , res) => {
    //Check Vaildation Data
    const { error, value } = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);


    //Check Email Exists
    const user = await User.findOne({email: value.email});
    if(! user) return  res.status(400).send("Email Not Exists");

    //Check Password Valid
    const passworValid = await bcrypt.compare(value.password , user.password);
    if(! passworValid) return res.status(400).send("Passord Invalid");

    const token = jwt.sign({user_id: user._id} , process.env.JWT_SECRET , {
        expiresIn: "2d"
        // expiresIn: 1 * 60 // it in 1 minutes
        // expiresIn: "10h" // it will be expired after 10 hours
        //expiresIn: "20d" // it will be expired after 20 days
       //expiresIn: 120 // it will be expired after 120ms
    });

    res.header('auth-token' , token).send(token);
});

module.exports = router;